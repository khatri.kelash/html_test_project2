import React, {Fragment} from 'react';
function index(props) {
    let {title, desc} = props;
    return ( //450x570
        <Fragment>
            <div className="blueinfo_bg">
                <div className='container '>
                    <div className='blueinfo_text_vertical_center'>
                        <div className='blueinfo_text_container'>
                            <h1 className='blueinfo_upper_text'>2500</h1>
                            <h1 className='blueinfo_lower_text'>HAPPY CLIENTS</h1>
                        </div>
                        <div className='blueinfo_text_container'>
                            <h1 className='blueinfo_upper_text'>300</h1>
                            <h1 className='blueinfo_lower_text'>FULL NOTEBOOKS</h1>
                        </div>
                        <div className='blueinfo_text_container'>
                            <h1 className='blueinfo_upper_text'>120</h1>
                            <h1 className='blueinfo_lower_text'>TEAMMATES</h1>
                        </div>
                        <div className='blueinfo_text_container'>
                            <h1 className='blueinfo_upper_text'>30</h1>
                            <h1 className='blueinfo_lower_text'>STORES</h1>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default index;