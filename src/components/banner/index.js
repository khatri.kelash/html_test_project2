import React, {Fragment} from 'react';
function index() {
    return (
        <Fragment>
            <div className="banner_bg">
                <div className='container '>
                    <div className='banner_text_position'>
                        <h1 className="banner_welcome">WELCOME TO</h1>
                        <h1 className='banner_comp_name'>YOURCOMPANYNAME</h1>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default index;
