import React from "react";

function index() {
    return (
            <section>
                <div className='all_aboutus_container' >
                    <div className='aboutus_inner_container'>
                        <div className='aboutus_column_container'>
                            <div className='aboutus_leftcolumn'>
                            </div>
                        </div>
                        <div className='aboutus_column_container'>
                            <div className='aboutus_rightcolumn'>
                                <div className='aboutus_rightcolumn_innercontainer'>
                                    <h1 className='aboutus_rightcolumn_title'>ABOUT US</h1>
                                    <h1 className='aboutus_rightcolumn_desc'>Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                        accusantium doloremque laudantium, totam rem aperiam </h1>
                                </div>
                                <div>
                                    <span className="dot"></span>
                                    <span className="dot"></span>
                                    <span className="dot"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    );
}

export default index;