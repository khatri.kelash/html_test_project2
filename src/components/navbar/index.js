import React, {Fragment} from 'react';

function index() {
    return (
        <Fragment>
            <ul>
                <div className='container'>
                    <li><a className="active" href="#">HOME</a></li>
                    <li><a href="#">ABOUT</a></li>
                    <li><a href="#">SERVICES</a></li>
                    <li><a href="#">TESTIMONIALS</a></li>
                    <li><a href="#">PORTFOLIO</a></li>
                    <li><a href="#">CONTACT</a></li>
                </div>
            </ul>
        </Fragment>
    );
}

export default index;
