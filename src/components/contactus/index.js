import React from "react";

function index() {
    return (
<section>
    <div className='contactus_bg'>
        <div className='container'>
            <div className='contactus_text_container'>
                <h1 className='contactus_title'>CONTACT US</h1>
                <h1 className='contactus_desc'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, </h1>
            </div>
            <div className='contactus_form_container'>
                <form>
                    <div className='contactus_form_item_container'>
                        <div className='contactus_form_row'>
                            <input type='text' name='name' placeholder='Name'/>
                            <select>
                                <option value="category">Category</option>
                            </select>
                        </div>
                        <div className='contactus_form_row'>
                            <input type='text' name='phone' placeholder='Phone'/>
                            <select>
                                <option value="city">City</option>
                            </select>
                        </div>
                        <div className='contactus_form_row'>
                            <input type='text' name='email' placeholder='E-mail'/>
                        </div>
                        <div className='contactus_form_row'>
                            <textarea  name='message' placeholder='Message' />
                        </div>
                    </div>
                    <div className='btn_container'>
                        <input type="submit" className='button' value="Contact us" />
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
    );
}

export default index;