import React, {Fragment} from 'react';
import Navigation from '../navbar/index';
import Banner from '../banner/index';
import Teamlead from '../teamlead/index';
import BlueInfoBar from '../blueinfobar/index';
import Contactus from '../contactus/index';
import Aboutus from '../aboutus/index';

function index() {
    let dummyDesc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore' ;
    return (
        <Fragment>
            <div className="companyName">
                <div className='container'>
                    <h1 className="companyName-Text">YOURCOMPANYNAME</h1>
                </div>
            </div>
            <Navigation />
            <Banner/>
            <section>
                <div className='leaders_intro'>
                    <div className='container'>
                        <div className='leaders_inner_container'>
                            <h1 className='leader_title'>OUR TEAM LEADERS</h1>
                            <hr className='hr_style' />
                            <h1 className='leader_desc'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                                voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur </h1>
                        </div>
                    </div>
                </div>
                <div className='all_teamleads_container'>
                    <div className='container'>
                        <Teamlead title ='Hugo Silva' desc = {dummyDesc} />
                        <Teamlead title ='Imanol Arias' desc = {dummyDesc} />
                        <Teamlead title ='Javier Bardem' desc = {dummyDesc} />
                    </div>
                </div>
            </section>
            <BlueInfoBar/>
            <div>
                <img className='img_area' src='assets/images/dummyimg.jpg'/>
            </div>
            <Contactus/>
            <Aboutus/>
        </Fragment>
    );
}

export default index;
