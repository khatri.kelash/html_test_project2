import React from 'react';
function index(props) {
    let {title, desc} = props;
    return ( //450x570
        <div className='teamlead_container'>
            <div className='teamlead_img'>
            </div>
            <div className='teamlead_img_text_container'>
                <h1 className='teamlead_img_title'>{title}</h1>
                <h1 className='teamlead_img_desc'>{desc}</h1>
            </div>
        </div>
    );
}

export default index;